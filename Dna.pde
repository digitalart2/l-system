class DNA
{
     float mutationRate;
     Galaxy galaxy;
     String[] axioms = { "F+F-FF", "F-F", "F++F-F++F", "F" };
     Rule[] rules = { new Rule( 'F', "FF+[+F-F-F]-[-F+F+F]" ), new Rule( 'F', "-F+F+[+F-F-]-[-F+F+F]" ),
                      new Rule( 'F', "F[+FF+F]" ), new Rule( 'F', "F[-FF+F]" ),
                      //  new Rule('F', "F[+F[F]+F]"),
                      new Rule( 'F', "F-F+F[+F-F]" ), new Rule( 'F', "-F+[FF++FF++FF]-F+" ),
                      new Rule( 'F', "-F-[FF-FF]+[FF+FF]+F-F-[FF+FF+F]+F+" ),
                      new Rule( 'F', "-F-[FF+FF]+[F]+FF-[-FFF]+[FF+FF]" ), new Rule( 'F', "FF+[-FF-F+F+F]F-" ),
                      new Rule( 'F', "F[+FF][-FF]F[-F][+F]F" ) };


     DNA( float mutationRate, Galaxy galaxy )
     {
          this.mutationRate = mutationRate;
          this.galaxy = galaxy;
     }

     // Метод для скрещивания ДНК
     void crossover( DNA partner )
     {
          this.mutationRate = ( this.mutationRate + partner.mutationRate ) / 2;// Простейший способ скрещивания
     }
     void copy( DNA other )
     {
          this.mutationRate = other.mutationRate;
          this.galaxy = other.galaxy;
     }


     float tranlatePosX = 1.0;
     int tranlatePosY = 0;
     // Метод для мутации ДНК
     void mutate()
     {
          if( random( 1 ) < this.mutationRate )
          {
               this.galaxy.sides = ( int )random( 2, 7 );
          }

          if( random( 1 ) > this.mutationRate )
          {
               this.galaxy.scaleBranch = random( 0.8, 1 );
          }
          else
          {
               this.galaxy.scaleBranch = 1;
          }

          if( random( 0.4 ) > this.mutationRate )
          {
               this.galaxy.angleBranch = ( int )random( 2, 7 );
          }
          else
          {
               this.galaxy.angleBranch = 2;
          }

          if( random( 1 ) > this.mutationRate )
          {
               this.galaxy.tranlatePosX = random( -100, 100 );
          }

          if( random( 1 ) > this.mutationRate )
          {
               this.galaxy.tranlatePosY = ( int )random( -200, 200 );
          }

          if( random( 1 ) > this.mutationRate )
          {
               this.galaxy.rules[ 0 ].setRule( rules[ int( random( rules.length ) ) ] );
               this.galaxy.axiom = axioms[ int( random( axioms.length ) ) ];
               this.galaxy.sentence = this.galaxy.axiom;
               int num;
               if( this.galaxy.rules[ 0 ].b.length() > 25 )
               {
                    num = 2;
               }
               else
               {
                    num = 3;
               }

               for( int i = 0; i < num; i++ )
               {
                    this.galaxy.generate();
               }
          }
     }
}
