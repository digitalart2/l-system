# L-system

## Introduction
Welcome to the L-System Generator! This program is designed to create unique L-Systems, fascinating structures that emerge through DNA crossover. L-Systems are a formal framework used to model and generate intricate patterns, such as fractals

## Contributing
If you're interested in contributing to this project, feel free to fork the repository, make changes, and create a pull request. We welcome contributions from developers of all levels!

## Examples of Generated Structures
![Пример L-системы](images/1.png)
![Пример L-системы](images/2.png)
![Пример L-системы](images/3.png)
