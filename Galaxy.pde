class Rule
{
     char a;
     String b;

     Rule( char a_, String b_ )
     {
          a = a_;
          b = b_;
     }
     void setRule( Rule other )
     {
          this.a = other.a;
          this.b = other.b;
     }

}

class Galaxy
{
     int sides = 5;
     float scaleFactor = 0.8;
     float scaleBranch = 1;
     int angleBranch = 2;
     float tranlatePosX = 1.0;
     int tranlatePosY = 0;
     int alpha = 0;
     String axiom = "F";
     String sentence = axiom;
     Rule[] rules = new Rule[ 1 ];
     int width;
     int height;
     float size;
     float spread = 2;
     DNA dna;
     boolean flag;
     float spread_inc = -0.03;
     boolean flag_reverse = false;
     boolean spreadInitialized = false;


     Galaxy( int w, int h )
     {
          this.width = w;
          this.height = h;
          this.dna = new DNA( random( 0, 1 ), this );
          rules[ 0 ] = new Rule( 'F', "-F-[FF+FF]+[F]+FF-[-FFF]+[FF+FF]" );
          size = min( width, height ) * 0.05;
          flag = false;
     }

     // Конструктор копирования
     void copy( Galaxy other )
     {
          this.sides = other.sides;
          this.scaleFactor = other.scaleFactor;
          this.scaleBranch = other.scaleBranch;
          this.angleBranch = other.angleBranch;
          this.tranlatePosX = other.tranlatePosX;
          this.tranlatePosY = other.tranlatePosY;
          this.alpha = other.alpha;
          this.axiom = other.axiom;
          this.sentence = other.sentence;
          this.size = other.size;
          this.spread = other.spread;
          this.spread_inc = other.spread_inc;
          this.flag_reverse = other.flag_reverse;
          this.spreadInitialized = other.spreadInitialized;
          // Копирование правил
          this.rules = new Rule[ other.rules.length ];
          for( int i = 0; i < other.rules.length; i++ )
          {
               this.rules[ i ] = new Rule( other.rules[ i ].a, other.rules[ i ].b );
          }
          this.dna.copy( other.dna );
     }

     void setRule( char a, String b )
     {
          this.rules[ 0 ].a = a;
          this.rules[ 0 ].b = b;
     }

     void generate()
     {
          String nextSentence = "";
          for( int i = 0; i < sentence.length(); i++ )
          {
               char current = sentence.charAt( i );
               boolean found = false;
               for( int j = 0; j < rules.length; j++ )
               {
                    if( current == rules[ j ].a )
                    {
                         found = true;
                         nextSentence += rules[ j ].b;
                         break;
                    }
               }
               if( !found )
               {
                    nextSentence += current;
               }
          }
          sentence = nextSentence;
     }

     void turtle()
     {
          pushMatrix();
          stroke( 255, 100 );

          for( int i = 0; i < sentence.length(); i++ )
          {

               char current = sentence.charAt( i );
               if( current == 'F' )
               {
                    Particle p = new Particle( tranlatePosX, tranlatePosY, size, this.alpha );
               }
               else if( current == '+' )
               {
                    // rotate(cp5.getController("spread").getValue());
                    rotate( this.spread );
               }
               else if( current == '-' )
               {
                    // rotate(-cp5.getController("spread").getValue());
                    rotate( -this.spread );
               }
               else if( current == '[' )
               {
                    pushMatrix();
               }
               else if( current == ']' )
               {
                    popMatrix();
               }
               else if( current == 'b' )
               {
                    translate( 0, -size );
               }
          }
          popMatrix();
     }

     void draw()
     {
          pushMatrix();
          translate( this.width / 2, this.height / 2 );
          for( int i = 0; i < sides; i++ )
          {
               translate( 0, 0 );
               scale( scaleBranch );
               rotate( PI * angleBranch / sides );
               this.turtle();
          }
          popMatrix();
     }

     // Метод для скрещивания ДНК
     void crossover( Galaxy partner )
     {
          this.dna.crossover( partner.dna );
          this.dna.mutate();
     }


     void animation()
     {
          if( this.spread < -1.7 )
          {
               this.spread_inc = -0.03;
               //this.flag_reverse = true;
               this.spread = 1.7;
               this.alpha = 0;
               this.flag = true;
               delay( 1000 );
          }
     }

     void update()
     {
          this.spread += this.spread_inc;
     }
}




//void animation()
//  {
//  if (this.spread < -1.7 && !this.spreadInitialized) {
          
//          this.alpha = 255;
//          delay(1500);
//          this.spread_inc = 0.03;
//          this.spreadInitialized = true;
      
//  }
    
//    if(this.spread > 1.7)
//    {
//      delay(500);
//      this.spread_inc = -0.03;
//      //this.flag_reverse = true;
//      this.spread = 1.699;
//      this.alpha = 0;
//      this.flag = true;
//      this.flag_reverse = false;
//      this.spreadInitialized = false;
      
      
//    }
//    //if(this.spread <= 1.5 && this.flag_reverse )
//    //{
//    //  this.alpha = 0;
//    //  this.flag = true;
//    //  this.flag_reverse = false;
//    //  this.spreadInitialized = false;
//    //}

//}
