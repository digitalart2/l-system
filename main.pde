import controlP5.*;
Galaxy gelaxy;
Galaxy gelaxySelection;
Galaxy buffer;
Timer timer;
ControlP5 cp5;
int iteration = 0;
int maxIteration;


void setup()
{
     //cp5 = new ControlP5(this);
     //cp5.addSlider("spread")
     //   .setPosition(20, 20)
     //   .setWidth(200)
     //   .setRange(-3, 3)
     //   .setValue(0.5) // Set an initial value for the spread slider
     //   .setLabel("Spread");

     //cp5.addSlider("sides")
     //   .setPosition(20, 50)
     //   .setWidth(200)
     //   .setRange(2, 12)
     //   .setValue(5) // Set an initial value for the sides slider
     //   .setLabel("Sides");

     //  cp5.addSlider("scaleFactor")
     //   .setPosition(20, 80)
     //   .setWidth(200)
     //   .setRange(0.78, 0.86) // Set the range for the scaleFactor slider
     //   .setValue(0.8) // Set an initial value for the scaleFactor slider
     //   .setLabel("Scale Factor");

     //  cp5.addSlider("scaleBranch") // Added the scaleBranch slider
     //   .setPosition(20, 110)
     //   .setWidth(200)
     //   .setRange(0.6, 1) // Set the range for the scaleBranch slider
     //   .setValue(1.0) // Set an initial value for the scaleBranch slider
     //   .setLabel("Scale Branch");

     //  cp5.addSlider("angleBranch") // Added the angleBranch slider
     //   .setPosition(20, 140)
     //   .setWidth(200)
     //   .setRange(1, 10) // Set the range for the angleBranch slider
     //   .setValue(2) // Set an initial value for the angleBranch slider
     //   .setLabel("Angle Branch");

     // cp5.addSlider("tranlatePosX") // Add the tranlatePosX slider
     //   .setPosition(20, 170)
     //   .setWidth(200)
     //   .setRange(-100, 100) // Set the range for the tranlatePosX slider
     //   .setValue(1.0) // Set an initial value for the tranlatePosX slider
     //   .setLabel("Translate Pos X");

     //cp5.addSlider("tranlatePosY") // Add the tranlatePosY slider
     //   .setPosition(20, 200)
     //   .setWidth(200)
     //   .setRange(-200, 200) // Set the range for the tranlatePosY slider
     //   .setValue(0) // Set an initial value for the tranlatePosY slider
     //   .setLabel("Translate Pos Y");

     //cp5.addSlider("alpha") // Новый слайдер для альфа-значения
     // .setPosition(20, 230) // Измените позицию слайдера, чтобы избежать перекрытия с другими элементами
     // .setWidth(200)
     // .setRange(0, 255) // Установите диапазон значений для альфа-канала (0 - полностью прозрачный, 255 - полностью непрозрачный)
     // .setValue(0) // Установите начальное значение для альфа-канала
     // .setLabel("Alpha"); // Установите метку для слайдера


     size( 1920, 1080 );// Enable 3D rendering with P3D

     gelaxy = new Galaxy( width, height );
     gelaxy.generate();
     gelaxy.generate();
     // gelaxy.generate();

     gelaxySelection = new Galaxy( width, height );
     buffer = new Galaxy( width, height );
     maxIteration = 3;
     timer = new Timer( 800 );
     timer.start();
}

void draw()
{
     background( 0 );
     if( !timer.isFinished() )
     {
          timer.update();
     }
     gelaxy.draw();
     if( timer.isFinished() && iteration < maxIteration )
     {
          nextGalaxy();
          timer.start();
     }

     if( iteration >= maxIteration )
     {
          //animation galaxy
          gelaxy.alpha = 255;
          gelaxy.animation();
          gelaxy.update();
          if( gelaxy.flag == true )
          {
               iteration = 0;
               maxIteration = 3;
               timer.start();
               gelaxy.flag = false;
          }
     }
}

void nextGalaxy()
{
     buffer.copy( gelaxy );
     gelaxy.crossover( gelaxySelection );
     gelaxySelection.copy( buffer );
     iteration += 1;
}


//void mousePressed() {
//if (mouseButton == RIGHT)
//{
//   Galaxy buffer = new Galaxy(width, height);
//   buffer.copy(gelaxy);
//   gelaxy.crossover(gelaxySelection);
//   gelaxySelection.copy(buffer);
//}
//}
