class Timer
{
     int startTime;
     int duration;
     boolean active;

     Timer( int duration )
     {
          this.duration = duration;
          active = false;
     }


     void start()
     {
          startTime = millis();
          active = true;
     }

     void update()
     {
          if( active )
          {
               int currentTime = millis();
               int elapsedTime = currentTime - startTime;

               if( elapsedTime >= duration )
               {
                    active = false;
               }
          }
     }

     boolean isFinished()
     {
          return !active;
     }
} 
